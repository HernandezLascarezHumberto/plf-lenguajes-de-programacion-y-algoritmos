# PROGRAMACION LOGICA Y FUNCIONAL ISB

## LENGUAJES DE PROGRAMACION Y ALGORITMOS

### 1.- PROGRAMANDO ORDENARORES EN LOS 80 Y AHORA. ¿QUE HA CAMBIADO?(2016)

```plantuml

@startmindmap

* Programando ordenadores en los 80 y ahora
** Ordenadores de los 80 - 90
*** Caracteristicas
**** Programación de bajo nivel
**** Velocidad de CPU se medía en MHz
*** Ventajas 
**** Programas mejor planificados
**** Menor cantidad de errores (bugs)
*** Desventajas
**** Recursos limitados
**** Mucho tiempo de Espera
**** Cuello de botella 
** Ordenadores actuales
*** Caracteristicas
**** Velocidad de CPU se mide en GHz
**** Programacion de alto y bajo nivel
**** Grandes cantidades de Memoria
*** Ventajas
**** Recursos ilimitados
**** Diversidad de plataformas
**** gran rendimiento del hardware
*** Desventajas
**** Mayor cantidad de errores
**** Mayor consumo de recursos 

@endmindmap
```

### 2.- Hª DE LOS ALGORITMOS Y DE LOS LENGUAJES DE PROGRAMACION

```plantuml

@startmindmap

* Historia de los algoritmos y\nde los lenguajes de programación.
** Algoritmo
***_ Aplicacion en la vida cotidiana
**** Manuales de usuario
**** Ordenes que resive un trabajador
***_ Secuencia de instrucciones
**** Instrucciones finitas
**** Llevadas a cabo para resolver un problema
***  Tipos de algoritmos
****  Razonables
***** El tiermpo de ejecucion crece despacio a medida de que los problemas crecen
**** No razonables
***** Llamadas
****** Exponenciales
****** Super polinominales

** Historia de los lenguajes de programaciones
*** Primera generacion de lenguajes
**** Código maquina
*** Segunda generacion de lenguajes
**** Lenguaje ensamblador
***** Anos 40's
*** Tercera generacion de lenguajes
**** Lenguajes de alto nivel
***** Año 1957 FORTRAN 
****** ORmula \n TRANslacion
******* Lenguaje orientado a resolver formulas matematicas
***** Año 1960 COBOL
****** Lenguaje de gestion 
***** 1963 PL/I
******_ Primer lenguaje que permitia
******* Multitarea
******* Programacion modular
***** 1964 BASIC
****** Lenguaje de programacion sensillo de aprender
***** 1964 LOGO
****** Lenguaje de programacion sencillo de aprender
***** 1970 PASCAL 
*** Cuarta generacion de lenguajes
**** 1972 C
***** Solo pocas instrucciones pueden traducir cada elemento del lenguajes
**** 1979 C++
***** Objetivo de extender al lenguaje de programacion C
**** 1991 HTML, PYTHON, VISUAL BASIC
***** Lenguajes de programacion enfocada a objetos
**** 1995 JAVA, JAVA SCRIP, PHP
***** Lenguajes que han definido la manera en la que entendemos el mundo
@endmindmap
```


### 3.- TENDENCIAS ACTUALES EN LOS LENGUAJES DE PROGRAMACION Y SISTEMAS INFORMATICOS. LA EVOLUCION DE LOS LENGUAJES Y PARADIGMAS DE PROGRAMACION (2005)

```plantuml

@startmindmap

* LA EVOLUCION DE LOS LENGUAJES \n Y PARADIGMAS DE PROGRAMACION
** Lenguaje de programacion
*** Lenguaje formal
*** Serie de instrucciones,le permite a un programador escribir un conjunto de ordenes
*** Lenguaje de programacion creados
**** 1950 - 1969
***** 1957 - FORTRAN 
***** 1958 - ALGOL
***** 1960 - LISP \n 1960 - COBOL
***** 1962 - APL
***** 1964 - BASIC
**** 1970
***** 1970 - PASCAL
***** 1972 - PROLOG \n 1972 - C 
***** 1975 -SCHEME \n 1975 - MODULA 
**** 1980
***** 1980 - SMALLTALK 
***** 1983 - OBJETIVE-C
***** 1986 - C++ 
***** 1987 - PERL
**** 1990
***** 1990 - HASKELL
***** 1991 - PYTHON
***** 1993 - RUBY 
***** 1995 - JAVA \n 1995 - RACKET
**** 2000
***** 2000 - C#
***** 2003 - SCALA \n 2003 - GROOVY 
***** 2009 - GO 
***** 2014 - SWIFT
** Paradigma de programación
*** Aproximacion a solucion de problemas
*** Manera de consebir la programación
** Paradigmas de programación
*** Estructurada
**** Lenguaje de medio nivel
**** Utilizacion de tres estructuras
***** Secuencia
***** Selecciones 
***** Interaccion
**** No soporta multiprogramación
***** Operaciones en paralelo
***** Sincronizaciones
*** Modular
**** Establecimiento de modulos
***** Requieren solucion de un problema
**** Divicion del programa, en pequeñas partes
***** Datos y procedimientos
***** Ocultos en el programa
*** Funcional
**** Evaluacion de expreciones
**** Definicion de funciones
**** Funciones como datos primitivos
**** Valores sin efectos laterales
**** Programacion declarativa
*** Lógico
**** Definicion de reglas
**** Unificacion como elementos de computación
**** Programación declarativa
*** Orientado a objetos
**** Definicion de clases y herencia
**** Objetos como abstraccion de datos y procedimientos
**** Polimorfismo
**** Chequeo de tipos de tiempo de ejecución
*** Imperativo o Procedual
**** Chequeo de tipos en tiempo de compilación
**** Cambio de estado de variables 
**** Pasos de ejecucion de un proceso

@endmindmap
```
